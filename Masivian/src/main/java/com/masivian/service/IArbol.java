/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.masivian.service;

import com.masivian.dto.Tree;

/**
 *
 * @author cballen
 */
public interface IArbol {

    /**
     * Crea un arbol
     *
     * @param tree
     * @return
     * @throws java.lang.Exception
     */
    public String crearArbol(Tree tree) throws Exception;

    /**
     * Busca un ancestro común entre 2 nodos
     *
     * @param tree
     * @return
     * @throws java.lang.Exception
     */
    public Integer retornarAncestro(Tree tree) throws Exception;
}
