/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.masivian.dto;


import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@XmlRootElement
public class Tree {
    private Nodo raiz;
    private List<Nodo> arbol;
    private Nodo nodoA;
    private Nodo nodoB;

    public Nodo getRaiz() {
        return raiz;
    }

    public void setRaiz(Nodo raiz) {
        this.raiz = raiz;
    }

    public List<Nodo> getArbol() {
        return arbol;
    }

    public void setArbol(List<Nodo> arbol) {
        this.arbol = arbol;
    }

    public Nodo getNodoA() {
        return nodoA;
    }

    public void setNodoA(Nodo nodoA) {
        this.nodoA = nodoA;
    }

    public Nodo getNodoB() {
        return nodoB;
    }

    public void setNodoB(Nodo nodoB) {
        this.nodoB = nodoB;
    }
    
    
}
